# Project Skeleton: Code More. Organize Less.

Nobody should have to spend half an hour moving around files to match the "recommended" directory structure. No one should have to choose between wasting time organizing code or leaving everything an unstructured mess. It should be easy and simple to create a file structure the way you want it with one command.

**Enter Project Skeleton.**

Project Skeleton is the easy way to organize your project the way you want it. Instead of typing out 20 `mkdir` commands to build a project directory, you can simply type:

`skelgen create [project]`

To get a local working copy of Project Skeleton, simply:

`git clone https://gitlab.com/bbworld1/project-skeleton.git`

`cd project-skeleton`

`cmake`

`make`

`sudo make install`

**Caveats:**
CMake 3.1 or higher is required.
A compiler supporting C++11 or higher is required.

# How to Use:

`skelgen create [template]` creates a new template within a project.
`skelgen destroy` deletes project
`skelgen nuke` also deletes project but sounds cooler

Custom templates may be specified in the ~/.local/share/skelgen directory, see docs/TEMPLATES.md for more details.

Example:
`skelgen create cmake` creates a new CMake project based on the template.


**Note:**
skelgen unfortunately currently has a very limited range of functionality, but we're working hard on adding some game-changing features. Stay tuned!
