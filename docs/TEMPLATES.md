# Skelgen Templates

`skelgen` creates its project structures based on default and user-defined templates. We include a few templates by default, but you are free to create your own and/or replace ours.

**Configuring a Template:**

Templates are stored in the folder `$HOME/.local/share/skelgen`. You may configure a template by creating a folder with your desired structure inside.

**Macros:**

`skelgen` supports a few template macros to make life easier: These macros are:

- `$PROJ` - the name of the project (will be derived from name of root folder, can be specified via `--name`)
- `$SRC_DIR_REL` - the RELATIVE path of the project directory
- `$SRC_DIR_ABS` - the ABSOLUTE path of the project directory

`skelgen` will automatically substitute the correct values for macros upon project creation.

**YAML Type Configs**

Optionally, you can create a YAML type config in the root of the template. This is read by `skelgen` to determine where to sort pre-existing files in the template. (NOTE: The `skelgen create` option works best when you have a type config set.)
