#ifndef __COMMANDS_H
#define __COMMANDS_H

#include <string>
using namespace std;

bool create(string spec);
bool destroy();
bool nuke();
bool push();
bool resolve();

#endif
