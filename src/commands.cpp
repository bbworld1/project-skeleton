#include "commands.h"

#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <string>
#include <iostream>
#include <fstream>
#include <cstdlib>

#include "yamlc.h"
#include "range.hpp"

// Get home directory
#ifdef _WIN32
	#define HOME "HOMEPATH"
#else
	#define HOME "HOME"
#endif

using namespace std;
namespace fs = boost::filesystem;

bool create(string spec) {
	/* Creates a new structure, automatically moving code where it needs to be.
         * spec: the template to use.
         */
	cout << "hi" << endl;
	cout << getenv(HOME) << endl;
	cout << "hi" << endl;
	if (!fs::exists(string(getenv(HOME)) + "/.local/share/skelgen/" + spec) ||
	    !fs::is_directory(string(getenv(HOME)) + "/.local/share/skelgen/" + spec)) {
	    	// Template is not valid (doesn't exist / is not directory)
		cerr << spec << " was not found as a valid template. You can add templates at ~/.local/share/skelgen/ ." << endl;
		return false;
	}

	fs::path sourceDir = fs::path(string(getenv(HOME)) + "/.local/share/skelgen/" + spec);
	fs::path destDir = fs::canonical(".");

	for (const auto& dir : fs::recursive_directory_iterator(sourceDir)) {
		const auto& path = dir.path();
		auto relativePathStr = path.string();
		boost::replace_first(relativePathStr, sourceDir.string(), "");
		fs::copy(path, destDir / relativePathStr);
	}
	return true;
}

bool organize() {
	/* Organize files based on maps.yaml configuration
	 */
	if (orderMappings("maps.yaml")) {
		return true;
	} else {
		return false;
	}
}

bool resolve() {
    /* Resolve macros
     */


    // Define macros here
    // May eventually be replaced with a user-editable conf file
    string proj;
    cout << "Enter a name for the project: ";
    cin >> proj;
    string curpath = fs::canonical(".").string();
    string macros[] = {"__PROJ", "__PATH", "__ABSPATH"};
    string replacements[] = {proj, curpath, curpath};
    vector<fs::path> fqueue;

    for (auto p: fs::directory_iterator(curpath)) {
        fqueue.push_back(p);
    }

    while (fqueue.size() > 0) {
        string file = fqueue.back().string();
        for (int i = 0; i < 3; i++) {
            boost::replace_all(file, macros[0], replacements[0]);
        }
        fs::rename(fqueue.back().string(), file);

        fqueue.pop_back();
        if (fs::is_directory(file)) {
            for (auto p: fs::directory_iterator(file)) {
                fqueue.push_back(p);
            }
            cout << "Adding " << file << endl;
        }
        else {
            if (fs::is_regular_file(file)) {
                ifstream ifile(file);
                ofstream tmp;
                string line;

                tmp.open("__tmp.txt");

                if (ifile.is_open()) {
                    while (getline(ifile, line)) {
                        for (int i = 0; i < 3; i++) {
                            boost::replace_all(line, macros[i], replacements[i]);
                        }
                        tmp << line;
                    }

                    ifile.close();
                    tmp.close();
                    fs::rename("__tmp.txt", file);
                }
            }
            else {
                cerr << "Could not read file." << endl;
                return false;
            }
        }
    }

    return true;
}

bool nuke() {
	for (auto& dir: fs::directory_iterator(fs::current_path())) {
		fs::remove_all(dir);
	}
	return true;
}

bool destroy() {
	vector<fs::path> paths = vector<fs::path>();
	for (auto& dir: fs::recursive_directory_iterator(".")) {
		if (!fs::is_directory(dir)) {
			paths.push_back(fs::path(dir));
		}
	}

	for (auto it = paths.begin(); it != paths.end(); ++it) {
		fs::rename(*it, it->filename());
	}

	for (auto& dir: fs::directory_iterator(fs::current_path())) {
		if (fs::is_directory(dir)) {
			fs::remove_all(dir);
		}
	}

	return true;
}

bool push() {
    int status = system("git init");
    string pushURL;
    if (status != 0) {
    	cerr << "Git is not installed, Could not init git repo." << endl;
    	return 1;
    }
    system("git add .");
    system("git commit -m 'Initial commit'");
    cout << "Enter a URL to push to (leave blank to setup later):";
    cin >> pushURL;
    if (pushURL.length() > 0) {
    	system(string(string("git remote add origin ") + pushURL).c_str());
    	system("git push -u origin master");
    }
    return true;
}
