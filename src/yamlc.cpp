#include <yaml-cpp/yaml.h>
#include <boost/filesystem.hpp>
#include <iostream>
#include <string>
using namespace std;
namespace fs = boost::filesystem;

bool orderMappings(string mapfile) {
	YAML::Node mappings = YAML::LoadFile(mapfile);
	
	for(auto it = mappings.begin(); it != mappings.end(); ++it) {

		fs::path dest = fs::canonical(it->second.as<string>());
		string ext = fs::extension(it->first.as<string>());
		vector<fs::path> paths = vector<fs::path>();
		cout << dest << endl;
		cout << endl;
		
		for(auto& p: fs::recursive_directory_iterator(".")) {
			if (fs::extension(p) == ext) {
				paths.push_back(fs::path(p));
			}
		}

		for(auto p = paths.begin(); p != paths.end(); ++p) {
			cout << p->filename() << " test" << endl;
			fs::rename(*p, dest / p->filename());
		}
		cout << endl;
	}

	return true;
}

