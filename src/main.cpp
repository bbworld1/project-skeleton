#include <iostream>
#include "cxxopts.hpp"
#include "commands.h"
#include <string>
using namespace std;

int main(int argc, char** argv) {
	cxxopts::Options options("skelgen", "Autogenerate skeletons for projects.");
	options.positional_help("command template");

	bool compile = false;
	// Add options
	options.add_options()
		("h,help", "Print the help")
		("git", "Initialize as a git repository")
        ("overwrite", "Delete old project before continuing");

	options.add_options("positional")
		("command", "Command", cxxopts::value<string>())
		("template", "Template", cxxopts::value<string>());

	// Parse positional separate from flags
	options.parse_positional({"command", "template"});
	int count = argc;
	auto result = options.parse(argc, argv);

	if (result.count("help") || count == 1) {
        cout << "Autogenerate and/or organize skeletons for projects." << endl;
		cout << "Usage:" << endl;
		cout << "  skelgen [OPTION...] command template" << endl;
		cout << endl;
		cout << "  Available commands:" << endl;
		cout << "    create - creates a new template" << endl;
		cout << "    nuke - deletes all project files" << endl;
		cout << "    destroy - removes all directory structure, making all files at top level" << endl;
		cout << endl;
		cout << "  Options:" << endl;
		cout << "  -h, --help  Print the help" << endl;
		cout << "  --git  Initialize as a Git repository" << endl;
		cout << "  --overwrite  Delete old project before continuing" << endl;
		cout << endl;
	return 0;
    }

	string command = "";
	if (result.count("command")) {
		command = result["command"].as<string>();
		if (command == "create") {
			if (result.count("template")) {
				create(result["template"].as<string>());
                resolve();
			} else {
				cerr << "Error: please specify a template." << endl;
			}
		} else if (command == "destroy") {
			destroy();
		} else if (command == "nuke") {
			nuke();
		} else if (command == "reorganize") {
            destroy();
			if (result.count("template")) {
				create(result["template"].as<string>());
				resolve();
			} else {
				cerr << "Error: please specify a template." << endl;
			}

        } else if (command == "push") {
            push();
	} else if (command == "resolve") {
        resolve();
        } else {
            cerr << command << " is not a valid command." << endl;
	}

	if (result.count("git")) {
        push();
	}

	return 0;
    }
}
